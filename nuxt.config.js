export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.TITLE,
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { hid: "og:type", property: "og:type", content: "website" },
      {
        hid: "og:title",
        property: "og:title",
        content: process.env.OG_TITLE
      },
      {
        hid: "og:description",
        property: "og:description",
        content: process.env.OG_DESCRIPTION
      },
      {
        hid: "og:image",
        property: "og:image",
        content: process.env.OG_IMAGE
      },
      { hid: "og:image:width", property: "og:image:width", content: "300" },
      { hid: "og:image:height", property: "og:image:height", content: "300" },
      { hid: "og:url", property: "og:url", content: process.env.DESTINATION },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/axios',
    'nuxt-user-agent',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/dayjs'
  ],

  dayjs: {
    plugins: [
      'utc',
      'timezone'
    ]
  },

  axios: {
    proxy: true
  },

  proxy: {
    '/api/': {
      target: process.env.API_URL + '/api/',
      pathRewrite: { "^/api/": "" }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  publicRuntimeConfig: {
    lineLiffId: process.env.LINE_LIFF_ID,
    lineNotifyToken: process.env.LINE_NOTIFY_TOKEN,
    baseUrl: process.env.BASE_URL,
    apiUrl: process.env.API_URL,
    destination: process.env.DESTINATION,
    ipApiToken: process.env.IPAPI_TOKEN
  },
}
